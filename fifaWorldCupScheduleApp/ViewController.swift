//
//  ViewController.swift
//  fifaWorldCupScheduleApp
//
//  Created by Sourav Dewett on 2019-03-07.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import UIKit
import WatchConnectivity
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    var index = 0
   
    var teamsList = [teams]()
    var isSubscribed : Bool = true
  
    @IBOutlet weak var tableView: UITableView!

//    override func viewDidAppear(_ animated: Bool) {
//
//        self.createTeams()
//        tableView.dataSource = self
//        tableView.delegate = self
//        tableView.reloadData()
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Awake View Controller")
        
        // Do any additional setup after loading the view, typically from a nib.
        
        self.createTeams()
        tableView.dataSource = self
        tableView.delegate = self

       }

    func createTeams() {
        
        
        
        let team1 = teams(Team1: "Chelsea", Team2: "Arsenal" , startTime: "15/06/2019", location: "London" , image1: "chelsea", image2: "001-arsenal")
        
        let team2 = teams(Team1: "Barcelona", Team2: "Manchester" , startTime: "16/06/2019", location: "London" , image1: "002-barcelona", image2: "003-manchester-united")

        let team3 = teams(Team1: "Real Madrid", Team2: "FC Bayern" , startTime: "17/06/2019", location: "London", image1: "004-real-madrid", image2: "005-bayern-munchen")

        let team4 = teams(Team1: "Chelsea", Team2: "Juvantis" , startTime: "18/06/2019", location: "London", image1: "chelsea", image2: "006-juventus")

        let team5 = teams(Team1: "LiverPool", Team2: "FC Dallas" , startTime: "19/06/2019", location: "London", image1: "007-liverpool", image2: "008-football")
        
        let team6 = teams(Team1: "Atlanta FC", Team2: "NewCastle Utd." , startTime: "20/06/2019", location: "London", image1: "009-pennant", image2: "010-pennant-1")
        
        teamsList.append(team1)
        teamsList.append(team2)
        teamsList.append(team3)
        teamsList.append(team4)
        teamsList.append(team5)
        teamsList.append(team6)
}


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let team = teamsList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! teamCell
        
        cell.setTeams(team: team)
        if(cell.subButton.titleLabel?.text == "Unsub")
        {
            if(WCSession.default.isReachable)
            {
                let message = ["Game": cell.Team1.text! + "VS" + cell.Team2.text!]
                WCSession.default.sendMessage(message, replyHandler: nil)
                
            }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        self.performSegue(withIdentifier: "sub", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
          let view = segue.destination as! subscribeViewController
        print(self.teamsList[index].Team1!)
       let a = self.teamsList[index].Team1!
        print(a)
             view.team1 = self.teamsList[index].Team1!
            view.team2 = self.teamsList[index].Team2
            view.location = self.teamsList[index].location!
            view.startTime = self.teamsList[index].startTime!
        view.image1 = self.teamsList[index].image1
        view.image2 = self.teamsList[index].image2

    }
    
    
}

