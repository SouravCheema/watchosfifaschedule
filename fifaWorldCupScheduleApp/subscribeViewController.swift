//
//  subscribeViewController.swift
//  fifaWorldCupScheduleApp
//
//  Created by Sourav Dewett on 2019-03-11.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import UIKit
import WatchConnectivity

class subscribeViewController: UIViewController,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    var team1: String?
    var team2: String?
    var location: String?
    var startTime:String?
    var image1: String?
    var image2: String?
    var Match: String?
    
    @IBOutlet weak var team1Label: UILabel!
    @IBOutlet weak var team2Label: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var teamImage1: UIImageView!
    @IBOutlet weak var teamImage2: UIImageView!
    
    @IBOutlet weak var subscribeButtonClicked: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        team1Label.text = team1
        team2Label.text = team2
        startTimeLabel.text = startTime
        locationLabel.text = location
        teamImage1.image = UIImage(named: image1!)
        teamImage2.image = UIImage(named: image2!)
        
        
        
        if (WCSession.isSupported()) {
            print("WC Session Active")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    

    @IBAction func subscribeButon(_ sender: Any) {
        
        if(subscribeButtonClicked.currentTitle == "Subscribe") {
            self.subscribed()
            self.subscribeButtonClicked.setTitle("Unsubscribe", for: .normal)
        } else {
            self.unscubscirbed()
            self.subscribeButtonClicked.setTitle("Subscribe", for: .normal)
        }
        
    }
    
    func subscribed() {
        if (WCSession.default.isReachable) {
            Match = team1! + " and " + team2! + " ON " + startTime!
            let message = ["SubscriptionStatus": "Subscribed: \(Match) "]
            
            WCSession.default.sendMessage(message, replyHandler: nil)
            
        }
    }
    
    func unscubscirbed() {
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            Match = team1! + " and " + team2! + " ON " + startTime!
            let message = ["SubscriptionStatus": "Unsubscribed: \(Match) )"]
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
            
        }
    }
    

}
