//
//  teamCell.swift
//  fifaWorldCupScheduleApp
//
//  Created by Sourav Dewett on 2019-03-10.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import UIKit
import WatchConnectivity
class teamCell: UITableViewCell{
    

  
    var isSubscribed: Bool = true
    
    @IBOutlet weak var Team1: UILabel!
    
    @IBOutlet weak var Team2: UILabel!
    
    @IBOutlet weak var startTime: UILabel!
    
    @IBOutlet weak var stadium: UILabel!
    
    @IBOutlet weak var subButton: UIButton!
    
    @IBOutlet weak var teamImage1: UIImageView!
    
    
    @IBOutlet weak var teamImage2: UIImageView!
    
    
    override func awakeFromNib() {
      
    }
    
    
    @IBAction func subButtonAction(_ sender: UIButton) {
        print("SubscribeButton Clicked")

        if(self.isSubscribed == true) {
            print("User Subscribed to this ")
            self.subButton.setTitle("Unsubscribe", for: .normal)
            self.isSubscribed = false
        } else {
            print("User Unsubbed")
            self.subButton.setTitle("Subscribe", for: .normal)
            self.isSubscribed = true
        }
       
        
        
    }
    
    
    
    func setTeams(team: teams) {
        
        Team1.text = team.Team1
        Team2.text = team.Team2
        startTime.text = team.startTime
        stadium.text = team.location
        teamImage1.image = UIImage(named: team.image1!)
        teamImage2.image = UIImage(named: team.image2!)
    }
}
