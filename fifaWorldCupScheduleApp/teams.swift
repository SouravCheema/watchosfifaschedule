//
//  teams.swift
//  fifaWorldCupScheduleApp
//
//  Created by Sourav Dewett on 2019-03-10.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import Foundation
import UIKit

class teams {
    
    var Team1 : String?
    var Team2 : String?
    var startTime: String?
    var location : String?
    var image1: String?
    var image2: String?
    
    convenience  init() {
        self.init(Team1: "", Team2: "", startTime: "", location: "",image1 : "",image2: "")
    }
    
    
    init(Team1: String, Team2:String, startTime:String, location:String, image1: String, image2: String) {
        self.Team1 = Team1
        self.Team2 = Team2
        self.startTime = startTime
        self.location = location
        self.image1 = image1
        self.image2 = image2
    }
    
}
