//
//  ScheduledGamesRowControllerInterfaceController.swift
//  fifaWorldCupScheduleApp WatchKit Extension
//
//  Created by Sourav Dewett on 2019-03-11.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import WatchKit
import Foundation


class ScheduledGamesRowControllerInterfaceController: WKInterfaceController {

    @IBOutlet weak var scheduledGamesTable: WKInterfaceTable!
    var teamsList:[gamesObject] = []
    
    func createTeams() {
        
        let team1 = gamesObject(Team1: "Chelsea", Team2: "Arsenal" , startTime: "15/06/2019", location: "London",image1: "chelsea", image2: "001-arsenal")
        
        let team2 = gamesObject(Team1: "Barcelona", Team2: "Manchester" , startTime: "16/06/2019", location: "London",image1: "002-barcelona", image2: "003-manchester-united")
        
        let team3 = gamesObject(Team1: "Real Madrid", Team2: "FC Bayern" , startTime: "17/06/2019", location: "London",image1: "004-real-madrid", image2: "005-bayern-munchen")
        
        let team4 = gamesObject(Team1: "Chelsea", Team2: "Juvantis" , startTime: "18/06/2019", location: "London", image1: "chelsea", image2: "006-juventus")
        
        let team5 = gamesObject(Team1: "LiverPool", Team2: "FC Dallas" , startTime: "19/06/2019", location: "London", image1: "007-liverpool", image2: "008-football")
        
        let team6 = gamesObject(Team1: "Atlanta FC", Team2: "NewCastle Utd." , startTime: "20/06/2019", location: "London", image1: "009-pennant", image2: "010-pennant-1")
        
        teamsList.append(team1)
        teamsList.append(team2)
        teamsList.append(team3)
        teamsList.append(team4)
        teamsList.append(team5)
        teamsList.append(team6)
        
    }
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.createTeams()
        
        
        self.scheduledGamesTable.setNumberOfRows(self.teamsList.count, withRowType: "myRow")
        
        for(i,game) in self.teamsList.enumerated() {
            let row = self.scheduledGamesTable.rowController(at: i) as! scheduledGamesRowController
            
            row.Team1Label.setText(game.Team1)
            row.Team2Label.setText(game.Team2)
            row.LocationLabel.setText(game.location)
            row.startTimeLabel.setText(game.startTime)
            row.team1Image.setImageNamed(game.image1)
            row.team2Image.setImageNamed(game.image2)
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
