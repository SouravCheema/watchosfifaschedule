//
//  InterfaceController.swift
//  fifaWorldCupScheduleApp WatchKit Extension
//
//  Created by Sourav Dewett on 2019-03-11.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

var messages:[String] = [""]

class InterfaceController: WKInterfaceController,WCSessionDelegate  {
    
    
    
    
    var message:String!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    
    // WKSession function used by our app
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        
        // output a debug message to the terminal
        print("Message Received")
        
            self.message = message["SubscriptionStatus"] as? String
       
        if(message["SubscriptionStatus"] as! String == "")
        {
            self.message = "No Game Subscribed"
        }
        
//        func subscribed() {
//            if (WCSession.default.isReachable) {
//
//                let message = ["SubscriptionEnabled": "Subscribed: \(Match) "]
//
//                WCSession.default.sendMessage(message, replyHandler: nil)
//
//            }
//        }
//
//        func unscubscirbed() {
//            if (WCSession.default.isReachable) {
//                // construct the message you want to send
//                // the message is in dictionary
//                let message = ["SubscribedGame": "Unsubscribed: \(Match) )"]
//                // send the message to the watch
//                WCSession.default.sendMessage(message, replyHandler: nil)
//
//            }
        print(self.message)
        messages.append(self.message!)
        //UserDefaults.standard.set(self.message!, forKey: "message")
        
        print(self.message!)
        print(messages)
    }
    
    
}
