//
//  subscribedGamesInterfaceController.swift
//  fifaWorldCupScheduleApp WatchKit Extension
//
//  Created by Sourav Dewett on 2019-03-11.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity



class subscribedGamesInterfaceController: WKInterfaceController {

    var gameList = messages
    
    @IBOutlet weak var subscribedGamesTable: WKInterfaceTable!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
//        self.subgameTable.setNumberOfRows(self.gameList.count, withRowType: "myRow2")
//        for(i,games) in self.gameList.enumerated() {
//            let row = self.subgameTable.rowController(at: i) as! rowController2
//            row.gameNameLabel.setText(games)
        
        self.subscribedGamesTable.setNumberOfRows(self.gameList.count, withRowType: "myRow1")
        
        for(i,game) in self.gameList.enumerated() {
            let row = self.subscribedGamesTable.rowController(at: i) as! subscribedGamesRowController
            
            row.subscribedGamesLabel.setText(game)
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
