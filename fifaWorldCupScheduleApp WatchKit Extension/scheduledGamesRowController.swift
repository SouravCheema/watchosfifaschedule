//
//  scheduledGamesRowController.swift
//  fifaWorldCupScheduleApp WatchKit Extension
//
//  Created by Sourav Dewett on 2019-03-11.
//  Copyright © 2019 Sourav Dewett. All rights reserved.
//

import WatchKit

class scheduledGamesRowController: NSObject {

    @IBOutlet weak var Team1Label: WKInterfaceLabel!
    
    @IBOutlet weak var Team2Label: WKInterfaceLabel!
    
    @IBOutlet weak var LocationLabel: WKInterfaceLabel!
    
    @IBOutlet weak var startTimeLabel: WKInterfaceLabel!
    
    @IBOutlet weak var team1Image: WKInterfaceImage!
    
    
    @IBOutlet weak var team2Image: WKInterfaceImage!
    
    
}
